package main

import (
	"strconv"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type People struct {
	UUID                    string  `sql:"type:uuid;primary_key" json:"uuid"`
	Survived                int     `binding:"required" json:"survived"`
	PassengerClass          int     `binding:"required" json:"passengerClass"`
	Name                    string  `binding:"required" json:"name"`
	Sex                     string  `binding:"required" json:"sex"`
	Age                     float64 `binding:"required" json:"age"`
	SiblingsOrSpousesAboard int     `binding:"required" json:"siblingsOrSpousesAboard"`
	ParentsOrChildrenAboard int     `binding:"required" json:"parentsOrChildrenAboard"`
	Fare                    float64 `binding:"required" json:"fare"`
}

func (People) TableName() string {
	return "people"
}

func (p *People) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.New()
	return scope.SetColumn("UUID", uuid.String())
}

func (p *People) Create(db *gorm.DB) error {
	err := db.Create(p).Error
	if err != nil {
		return err
	}

	return nil
}

func ListPeople(db *gorm.DB) (people []People, err error) {
	err = db.Find(&people).Error
	if err != nil {
		return nil, err
	}

	return people, nil
}

func GetPeople(db *gorm.DB, uuid string) (people People, err error) {
	err = db.Where(&People{UUID: uuid}).First(&people).Error
	if err != nil {
		return people, err
	}

	return people, nil
}

func (p *People) Update(db *gorm.DB) error {
	err := db.Save(p).Error
	if err != nil {
		return err
	}

	return nil
}

func (p *People) Delete(db *gorm.DB) error {
	err := db.Delete(p).Error
	if err != nil {
		return err
	}

	return nil
}

func (p *People) LoadCSVRecord(record []string) (err error) {
	p.Survived, err = strconv.Atoi(record[0])
	if err != nil {
		return err
	}

	p.PassengerClass, err = strconv.Atoi(record[1])
	if err != nil {
		return err
	}

	p.Name = record[2]
	p.Sex = record[3]

	p.Age, err = strconv.ParseFloat(record[4], 64)
	if err != nil {
		return err
	}

	p.SiblingsOrSpousesAboard, err = strconv.Atoi(record[5])
	if err != nil {
		return err
	}

	p.ParentsOrChildrenAboard, err = strconv.Atoi(record[6])
	if err != nil {
		return err
	}

	p.Fare, err = strconv.ParseFloat(record[7], 64)
	if err != nil {
		return err
	}

	return nil
}
