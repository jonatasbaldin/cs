package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetCSVRecordsFromFile(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		_, err := GetCSVRecordsFromFile("./fixtures/good_csv.csv")
		assert.Nil(t, err)
	})

	t.Run("file not found", func(t *testing.T) {
		_, err := GetCSVRecordsFromFile("say what")
		assert.EqualError(t, err, "open say what: no such file or directory")
	})

	t.Run("bad csv file", func(t *testing.T) {
		_, err := GetCSVRecordsFromFile("fixtures/bad_csv.csv")
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "parse error")
	})
}
