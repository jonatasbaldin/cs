package main

import (
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func Test_PeopleModel(t *testing.T) {
	record := []string{"1", "1", "Mrs. Jacques", "female", "35", "1", "0", "53.1"}

	t.Run("load CSV record", func(t *testing.T) {
		p := People{}
		err := p.LoadCSVRecord(record)

		assert.Nil(t, err)

		assert.Equal(t, p.Survived, 1)
		assert.Equal(t, p.PassengerClass, 1)
		assert.Equal(t, p.Name, "Mrs. Jacques")
		assert.Equal(t, p.Sex, "female")
		assert.Equal(t, p.Age, 35.0)
		assert.Equal(t, p.SiblingsOrSpousesAboard, 1)
		assert.Equal(t, p.ParentsOrChildrenAboard, 0)
		assert.Equal(t, p.Fare, 53.1)
	})

	t.Run("create people successful", func(t *testing.T) {
		fakeDB, _ := gorm.Open("sqlite3", ":memory:")
		fakeDB.AutoMigrate(&People{})

		people := People{}
		people.LoadCSVRecord(record)
		result := People{}

		err := people.Create(fakeDB)
		fakeDB.First(&result)

		assert.Nil(t, err)
		assert.Equal(t, result, people)
	})

	t.Run("list people successful", func(t *testing.T) {
		fakeDB, _ := gorm.Open("sqlite3", ":memory:")
		fakeDB.AutoMigrate(&People{})

		people1 := People{}
		people2 := People{}
		people1.LoadCSVRecord(record)
		people2.LoadCSVRecord(record)

		people1.Create(fakeDB)
		people2.Create(fakeDB)

		result, err := ListPeople(fakeDB)

		assert.Nil(t, err)
		assert.Len(t, result, 2)
		assert.Equal(t, result[0], people1)
		assert.Equal(t, result[1], people2)
	})

	t.Run("get people successful", func(t *testing.T) {
		fakeDB, _ := gorm.Open("sqlite3", ":memory:")
		fakeDB.AutoMigrate(&People{})

		people := People{}
		people.LoadCSVRecord(record)
		people.Create(fakeDB)

		result, err := GetPeople(fakeDB, people.UUID)

		assert.Nil(t, err)
		assert.Equal(t, people, result)
	})

	t.Run("update people successful", func(t *testing.T) {
		fakeDB, _ := gorm.Open("sqlite3", ":memory:")
		fakeDB.AutoMigrate(&People{})

		people := People{}
		people.LoadCSVRecord(record)
		people.Create(fakeDB)
		people.Survived = 100

		people.Update(fakeDB)

		result, err := GetPeople(fakeDB, people.UUID)

		assert.Nil(t, err)
		assert.Equal(t, people, result)
	})

	t.Run("delete people successful", func(t *testing.T) {
		fakeDB, _ := gorm.Open("sqlite3", ":memory:")
		fakeDB.AutoMigrate(&People{})

		people := People{}
		people.LoadCSVRecord(record)
		people.Create(fakeDB)

		people.Delete(fakeDB)

		_, err := GetPeople(fakeDB, people.UUID)

		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "record not found")
	})
}
