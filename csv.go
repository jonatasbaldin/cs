package main

import (
	"encoding/csv"
	"os"
)

//GetCSVRecordsFromFile returns all the records from a CSV file, excluding its header
func GetCSVRecordsFromFile(filename string) (records [][]string, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return records, err
	}
	defer file.Close()

	r := csv.NewReader(file)

	records, err = r.ReadAll()
	if err != nil {
		return records, err
	}

	return records[1:], err
}
