package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	csvFilePath := flag.String("import", "", "import CSV")
	migrateDB := flag.Bool("migrate-db", false, "migrate DB")
	runServer := flag.Bool("run-server", false, "run HTTP server")
	flag.Parse()

	if len(os.Args) <= 1 {
		flag.Usage()
	}

	config, err := NewConfig()
	if err != nil {
		log.Fatalf("could not initialize config: %v", err)
	}

	err = config.NewDBConn()
	if err != nil {
		log.Fatalf("could not connect to the database: %v", err)
	}

	if *migrateDB {
		err := config.DB.AutoMigrate(&People{}).Error
		if err != nil {
			log.Fatalf("could not migrate database: %v", err)
		}

		log.Printf("database migrated")
		os.Exit(0)
	}

	if *csvFilePath != "" {
		records, err := GetCSVRecordsFromFile(*csvFilePath)
		if err != nil {
			log.Fatalf("could not read CSV file: %v", err)
		}

		for _, record := range records {
			people := People{}
			err := people.LoadCSVRecord(record)
			if err != nil {
				log.Fatalf("could not load CSV record into People: %v", err)
			}

			err = people.Create(config.DB)
			if err != nil {
				log.Printf("could not create People instance '%v': %v", people, err)
			}
		}

		log.Printf("CSV file %q imported", *csvFilePath)
	}

	if *runServer {
		router := SetupRouter(config)

		err := router.Run(fmt.Sprintf(":%s", config.PORT))
		if err != nil {
			log.Fatalf("could not start webserver: %v", err)
		}
	}
}
