package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Config(t *testing.T) {
	os.Setenv("CS_DBHOSTNAME", "hostname")
	os.Setenv("CS_DBPORT", "port")
	os.Setenv("CS_DBUSER", "user")
	os.Setenv("CS_DBPASS", "pass")
	os.Setenv("CS_DBSSL", "disable")

	c, err := NewConfig()

	assert.Nil(t, err)
	assert.Equal(t, c.DBHostName, "hostname")
	assert.Equal(t, c.DBPort, "port")
	assert.Equal(t, c.DBUser, "user")
	assert.Equal(t, c.DBPass, "pass")
	assert.Equal(t, c.DBSSL, "disable")
	assert.Equal(t, c.PORT, "8080")
}
