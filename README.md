# Container Solutions Challenge

## Tech Stack
- Golang
- PostgreSQL
- pgweb (for SQL debugging)
- docker-compose
- Kubernetes

## Testing
```bash
$ make test
```

## Running Locally with docker-compose
Create a .env file with the following environment variables:
```bash
CS_DBHOSTNAME=db
CS_DBPORT=5432
CS_DBUSER=postgres     
CS_DBPASS=postgres
CS_DBSSL=disable      
CS_PORT=8080
```

Deploy using the following commands:
```bash
$ make run-local
$ make import-csv
```

Connect to `http://localhost:8080/people`.

## Running Locally without docker-compose
Create a .env file with the following environment variables:
```bash
CS_DBHOSTNAME=localhost
CS_DBPORT=5432
CS_DBUSER=postgres     
CS_DBPASS=postgres
CS_DBSSL=disable      
CS_PORT=8080
```

```bash
$ docker-compose up -d db pgweb
$ go build
$ ./cs -migrate-db
$ ./cs -import contrib/titanic.csv
$ ./cs -run-server
```

## Deploying to Kubernetes
```bash
# Create a secret for the PostgreSQL password
$ kubectl create secret generic postgres --from-literal=password=<password here>

$ make deploy-k8s
```
## Improvements
Here are a couple of improvements that could still be done. This list is not exhaustive, just an idea for what I could still work on:

### Infrastructure
- Create a Helm chart for the K8S deployment, where we could change the values from the YAML files dynamically 
- Implement HA in the PostgreSQL database by extending the `StatefulSet` or using a proper operator
- Implement a `/healthz` endpoint for readiness
- Add an Ingress to expose to the Internet
- Create a Docker manifest (experimental) for multi arch images if necessary

### Code
- Add structured logs
- Watcher to rebuild the `cs` image locally when code is changed, to easy local development
- Create custom validations for POST/PUT, according to any necessary business rules
- Improve HTTP handlers error validation to expose multiple errors (like when validation fails on POST)
- Test all error cases in Test_PeopleModel/load_CSV_record
- Mock filesystem in Test_GetCSVRecordsFromFile

## Discussions
- Since the API is called `people`, I've used this term everywhere, even in a singular context, like `GetPeople` returning just one item. I have no strong opinions on this one due the size of the project, but it would be one thing I'd discuss with the team.

## Questions and Feedback
If you have any questions of feedback, please let me know :)