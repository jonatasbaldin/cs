package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func setup() (*Config, *gin.Engine) {
	fakeDB, _ := gorm.Open("sqlite3", ":memory:")
	fakeDB.AutoMigrate(&People{})

	config, _ := NewConfig()
	config.DB = fakeDB

	gin.SetMode("test")
	router := SetupRouter(config)

	return config, router
}

func request(router *gin.Engine, method string, path string, body io.Reader) *httptest.ResponseRecorder {
	req, err := http.NewRequest(method, path, body)
	if err != nil {
	}

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr

}

func Test_ListPeopleHandler(t *testing.T) {
	config, router := setup()
	record := []string{"1", "1", "Mrs. Jacques", "female", "35", "1", "0", "53.1"}

	people := People{}
	people.LoadCSVRecord(record)
	people.Create(config.DB)

	r := request(router, "GET", "/people", nil)

	result := []People{}
	json.Unmarshal(r.Body.Bytes(), &result)

	assert.Equal(t, http.StatusOK, r.Code)
	assert.Equal(t, people, result[0])
}

func Test_GetPeopleHandler(t *testing.T) {
	config, router := setup()
	record := []string{"1", "1", "Mrs. Jacques", "female", "35", "1", "0", "53.1"}

	people := People{}
	people.LoadCSVRecord(record)
	people.Create(config.DB)

	t.Run("successfull", func(t *testing.T) {
		route := fmt.Sprintf("/people/%s", people.UUID)
		r := request(router, "GET", route, nil)

		result := People{}
		json.Unmarshal(r.Body.Bytes(), &result)

		assert.Equal(t, http.StatusOK, r.Code)
		assert.Equal(t, people, result)
	})

	t.Run("not found", func(t *testing.T) {
		route := fmt.Sprintf("/people/%s", uuid.New().String())
		r := request(router, "GET", route, nil)

		result := make(map[string]interface{})
		json.Unmarshal(r.Body.Bytes(), &result)

		assert.Equal(t, http.StatusNotFound, r.Code)
		assert.Equal(t, "not found", result["error"])
	})
}

func Test_CreatePeopleHandler(t *testing.T) {
	config, router := setup()

	t.Run("successfull", func(t *testing.T) {
		body := []byte(`{"survived":10,"passengerClass":2,"name":"test","sex":"male","age":10,"siblingsOrSpousesAboard":1,"parentsOrChildrenAboard":2,"fare":0.1}`)
		r := request(router, "POST", "/people", bytes.NewBuffer(body))

		result := People{}
		json.Unmarshal(r.Body.Bytes(), &result)

		expectedResult := People{}
		config.DB.First(&expectedResult)

		assert.Equal(t, http.StatusCreated, r.Code)
		assert.Equal(t, expectedResult, result)
	})

	t.Run("error missing field", func(t *testing.T) {
		body := []byte(`{"passengerClass":2,"name":"test","sex":"male","age":10,"siblingsOrSpousesAboard":1,"parentsOrChildrenAboard":2,"fare":0.1}`)
		r := request(router, "POST", "/people", bytes.NewBuffer(body))

		assert.Equal(t, http.StatusInternalServerError, r.Code)
	})

}

func Test_UpdatePeopleHandler(t *testing.T) {
	config, router := setup()
	record := []string{"1", "1", "Mrs. Jacques", "female", "35", "1", "0", "53.1"}

	people := People{}
	people.LoadCSVRecord(record)
	people.Create(config.DB)

	t.Run("successfull", func(t *testing.T) {
		body := []byte(`{"survived":10}`)
		route := fmt.Sprintf("/people/%s", people.UUID)
		r := request(router, "PUT", route, bytes.NewBuffer(body))

		result := People{}
		json.Unmarshal(r.Body.Bytes(), &result)

		expectedResult := People{}
		config.DB.First(&expectedResult)

		assert.Equal(t, http.StatusOK, r.Code)
		assert.Equal(t, expectedResult, result)
	})

	t.Run("not found", func(t *testing.T) {
		body := []byte(`{"passengerClass":2}`)
		route := fmt.Sprintf("/people/%s", uuid.New().String())
		r := request(router, "PUT", route, bytes.NewBuffer(body))

		assert.Equal(t, http.StatusNotFound, r.Code)
	})
}

func Test_DeletePeopleHandler(t *testing.T) {
	config, router := setup()
	record := []string{"1", "1", "Mrs. Jacques", "female", "35", "1", "0", "53.1"}

	people := People{}
	people.LoadCSVRecord(record)
	people.Create(config.DB)

	t.Run("successfull", func(t *testing.T) {
		route := fmt.Sprintf("/people/%s", people.UUID)
		r := request(router, "DELETE", route, nil)

		assert.Equal(t, http.StatusNoContent, r.Code)
	})

	t.Run("not found", func(t *testing.T) {
		route := fmt.Sprintf("/people/%s", uuid.New().String())
		r := request(router, "DELETE", route, nil)

		assert.Equal(t, http.StatusNotFound, r.Code)
	})
}
