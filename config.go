package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/kelseyhightower/envconfig"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Config struct {
	DB *gorm.DB

	DBHostName string `required:"true"`
	DBPort     string `required:"true"`
	DBUser     string `required:"true"`
	DBPass     string `required:"true"`
	DBSSL      string `required:"true"`
	PORT       string `default:"8080"`
}

func NewConfig() (*Config, error) {
	var config Config
	err := envconfig.Process("cs", &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func (c *Config) NewDBConn() error {
	dbConn := fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=%s",
		c.DBHostName,
		c.DBPort,
		c.DBUser,
		c.DBPass,
		c.DBSSL,
	)

	db, err := gorm.Open("postgres", dbConn)
	if err != nil {
		return err
	}

	c.DB = db

	return nil
}
