package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func ListPeopleHandler(config *Config) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		people, err := ListPeople(config.DB)

		if err != nil {
			log.Printf("could not list people: %v", err)
			c.JSON(http.StatusBadGateway, httpError("could not list people"))
			return
		}

		c.JSON(http.StatusOK, people)
	}

	return fn
}

func GetPeopleHandler(config *Config) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		uuid := c.Param("uuid")
		people, err := GetPeople(config.DB, uuid)

		if gorm.IsRecordNotFoundError(err) {
			c.JSON(http.StatusNotFound, httpError("not found"))
			return
		}

		if err != nil {
			log.Printf("could not get people: %v", err)
			c.JSON(http.StatusInternalServerError, httpError("could not get people"))
			return
		}

		c.JSON(http.StatusOK, people)
	}
	return fn
}

func CreatePeopleHandler(config *Config) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		people := People{}
		err := c.ShouldBindJSON(&people)

		if err != nil {
			log.Printf("could not create people: %v", err)
			c.JSON(http.StatusInternalServerError, httpError("could not create people"))
			return
		}

		err = people.Create(config.DB)

		if err != nil {
			log.Printf("could not create people: %v", err)
			c.JSON(http.StatusBadRequest, httpError("could not create people"))
			return
		}

		c.JSON(http.StatusCreated, people)
	}

	return fn
}

func UpdatePeopleHandler(config *Config) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		uuid := c.Param("uuid")
		people, err := GetPeople(config.DB, uuid)

		if gorm.IsRecordNotFoundError(err) {
			c.JSON(http.StatusNotFound, httpError("not found"))
			return
		}

		err = json.NewDecoder(c.Request.Body).Decode(&people)

		if err != nil {
			log.Printf("could not update people: %v", err)
			c.JSON(http.StatusInternalServerError, httpError("could not update people"))
			return
		}

		err = people.Update(config.DB)

		if err != nil {
			log.Printf("could not update people: %v", err)
			c.JSON(http.StatusBadRequest, httpError("could not update people"))
			return
		}

		c.JSON(http.StatusOK, people)
	}

	return fn
}

func DeletePeopleHandler(config *Config) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		uuid := c.Param("uuid")
		people, err := GetPeople(config.DB, uuid)

		if gorm.IsRecordNotFoundError(err) {
			c.JSON(http.StatusNotFound, httpError("not found"))
			return
		}

		err = people.Delete(config.DB)

		if err != nil {
			log.Printf("could not delete people: %v", err)
			c.JSON(http.StatusInternalServerError, httpError("could not delete people"))
			return
		}

		c.JSON(http.StatusNoContent, nil)
	}

	return fn
}

func SetupRouter(config *Config) *gin.Engine {
	router := gin.Default()
	router.GET("/people", ListPeopleHandler(config))
	router.GET("/people/:uuid", GetPeopleHandler(config))
	router.POST("/people", CreatePeopleHandler(config))
	router.PUT("/people/:uuid", UpdatePeopleHandler(config))
	router.DELETE("/people/:uuid", DeletePeopleHandler(config))

	return router
}

func httpError(message string) map[string]interface{} {
	err := make(map[string]interface{})
	err["error"] = message

	return err
}
