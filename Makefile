build:
	docker-compose build

push:
	docker-compuse push

test:
	go test ./...

run-local:
	docker-compose up -d db pgweb
	# docker-compose should have a good healthy/restart system!
	# instead of using sleep here, I could use something like the wait-for-it.sh wrapper
	# but I'd need to sacrifice the `FROM scratch` image
	# since docker-compose is just for local development, I'd prefer to be hacky here
	sleep 5
	docker-compose run cs -migrate-db
	docker-compose up -d cs

import-csv:
	docker-compose run cs -import titanic.csv

stop:
	docker-compose stop

deploy-k8s:
	kubectl apply -f k8s/postgres.yaml 
	kubectl rollout status statefulset postgres

	kubectl apply -f k8s/cs.yaml
	kubectl rollout status deployment cs

	kubectl apply -f k8s/cs-job.yaml

	# ##################################################################################################
	# Connect to the service via port-forward using the following command
	# kubectl port-forward $$(kubectl get pod -l app=cs --no-headers --output name | head -n1) 8080:8080
	# ##################################################################################################

clean-k8s:
	kubectl delete -f k8s/cs-job.yaml
	kubectl delete -f k8s/cs.yaml
	kubectl delete -f k8s/postgres.yaml 