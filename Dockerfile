FROM golang:1.13 as build

WORKDIR /app
COPY . .
 
RUN go test ./...
RUN CGO_ENABLED=0 GOOS=linux go build --ldflags "-s -w" -o cs

FROM scratch
COPY --chown=65534:0 --from=build /app/cs /
COPY --from=build /app/contrib/titanic.csv /

# user 'nobody'
USER 65534

EXPOSE 8080

ENTRYPOINT ["/cs"]
CMD ["/cs"]